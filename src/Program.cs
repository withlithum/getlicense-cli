﻿/**
* getLicense
  Copyright (C) 2021  WithLithum.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using WithLithum.GetLicense;
using System.Net;
using System.Net.Http;

if (args.Length != 1)
{
  return Util.ReturnCode("incorrect params", 1);
}

if (!File.Exists("licenselist.json"))
{
  return Util.ReturnCode("no license list", 2);
}

// parse
var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText("licenselist.json"));

if (dictionary == null)
{
  return Util.ReturnCode("license list null", 2);
}

if (!dictionary.ContainsKey(args[0]))
{
  return Util.ReturnCode("no such license", 3);
}

var client = new HttpClient();

var task = client.GetStringAsync(dictionary[args[0]]);
task.Wait();

File.WriteAllText("LICENSE", task.Result);

Console.WriteLine("License file written");

return 0;